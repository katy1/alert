from selenium import webdriver
import unittest
from page.PageMenu import PageMenu
from page.PageMain import PageMain
from MethodMain import MethodMain
import time

class TestCase(unittest.TestCase):
    def setUp(self):
        self.page_menu = PageMenu()
        self.page_main = PageMain()
        self.page_method_main = MethodMain()
        self.driver = webdriver.Firefox()

    def test_cursor(self):
        driver = self.driver
        driver.get("http://way2automation.com/way2auto_jquery/frames-and-windows.php")
        driver.switch_to_window(driver.window_handles[-1])
        self.page_method_main.authorization(self.driver)
        self.driver.refresh()
        self.page_main.click_menu_alert(driver)
        self.page_menu.click_input_alert(driver)
        self.page_menu.set_frame(driver)
        self.page_menu.click_button_alert(driver)
        driver.switch_to_alert()
        driver.switch_to_alert().send_keys("katy")
        driver.switch_to_alert().accept()
        text_standart = "Hello katy! How are you today?"
        text1 = self.page_menu.set_text(driver)
        self.assertEquals(text_standart, text1)

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
