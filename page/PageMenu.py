# -*- coding: utf8 -*-
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

class PageMenu(object):
    _locators = {
        'input_alert': "//ul[@class='responsive-tabs']/li[2]/a",
        'frame': "//*[@id='example-1-tab-2']//iframe",
        'button_alert': "//button",
        'text_demo': "demo"
    }

    def __find_button(self, driver, path_element):
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def __find_element(self, driver, path_element):
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def click_input_alert(self, driver):
        self.__find_button(
            driver, self._locators['input_alert']).click()

    def set_frame(self, driver):
        frame = self.__find_element(
            driver, self._locators['frame'])
        driver.switch_to_frame(frame)

    def click_button_alert(self, driver):
        link = self.__find_button(driver, self._locators['button_alert']).click()
        return link

    def set_text(self, driver):
        text_demo = driver.find_element_by_id(self._locators['text_demo']).get_attribute("innerText")
        return text_demo
